'''
Wrapper for the Docker tool larsjuhljensen/tagger
'''
import os
import sys
import shlex
from subprocess import Popen, PIPE, STDOUT
from io import StringIO

import numpy as np
import pandas as pd

# cat test_documents.tsv | docker run -i -v $(pwd):/data larsjuhljensen/tagger \
#     --entities=test_entities.tsv \
#     --names=test_names.tsv \
#     --types=test_types.tsv 

# docker run -v $(pwd):/data larsjuhljensen/tagger \
#     --documents=test_documents.tsv \
#     --entities=test_entities.tsv \
#     --names=test_names.tsv \
#     --types=test_types.tsv 
#     # > output.tsv

def make_document(records):
    '''
    Make a document conforming the input of tagger:
    The format of the input files are tab separated columns that contain the following data:
    - identifier
    - authors
    - journal
    - year
    - title
    - text

    @params:
        - records: a iterator from Medline.parse(handle)
    '''
    required_fields = set(['PMID', 'TI', 'AB'])
    docs = [(
        'PMID:%s' % rec['PMID'], # identifier
        'authors', # authors
        rec.get('JT', 'None'), # journal
        rec.get('EDAT', 'None'), # year
        rec['TI'], # title
        rec['AB'] # text
        ) for rec in records if required_fields.issubset(set(rec.keys())) ]

    docs_stream = [None] * len(docs)
    for i, doc in enumerate(docs):
        docs_stream[i] = '\t'.join(doc)
    docs_stream = '\n'.join(docs_stream) + '\n'
    return docs_stream
    
def call_tagger(docs_stream, 
    tagger_dir='.',
    entities=None, 
    names=None,
    types=None,
    threads=1,
    stopwords=None,
    groups=None,

    ):
    '''
    @params:
        - docs_stream as STDIN for tagger
        - tagger_dir is the directory that will be mounted. 
        - entities, names, types should be filenames in tagger_dir
    '''
    cmd = "docker run -i -v %s:/data larsjuhljensen/tagger " % os.path.abspath(tagger_dir) +\
    "--entities=%s " % entities +\
    "--names=%s " % names +\
    "--types=%s " % types +\
    "--threads=%d" % threads
    if stopwords:
        cmd += ' --stopwords=%s' % stopwords
    if groups:
        cmd += ' --groups=%s' % groups

    print(cmd)
    proc = Popen(shlex.split(cmd), stdout=PIPE, stdin=PIPE, stderr=PIPE)
    output, err = proc.communicate(input=docs_stream.encode('utf-8'))
    return output, err


def parse_output(output, err):
    print(err)
    filestream = StringIO(output.decode())
    df = pd.read_csv(filestream, sep='\t', 
        names=['pmid', 
            'paragraph_number',
            'sentence_number',
            'first_char',
            'last_char',
            'term_matched',
            'species_taxid',
            'serialno'
        ])
    return df


