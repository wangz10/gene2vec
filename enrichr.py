'''
Utils to get data from Enrichr
'''
import os
import json
import urllib.request
import requests
import pandas as pd
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer, TfidfTransformer

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(SCRIPT_DIR, '../data/Enrichr')

def get_enrichr_meta():
    url = 'http://amp.pharm.mssm.edu/Enrichr/datasetStatistics'
    resp = requests.get(url)
    resp.raise_for_status()
    return pd.DataFrame(resp.json()['statistics'])

def download_library(name):
    url = 'http://amp.pharm.mssm.edu/Enrichr/geneSetLibrary?mode=text&libraryName=%s' % name
    filename = '%s.gmt'% name
    filepath = os.path.join(DATA_DIR, filename)
    urllib.request.urlretrieve(url, filepath)
    return filepath

def get_library_filepath(name):
    '''Download the file if not exists.'''
    filename = '%s.gmt'% name
    filepath = os.path.join(DATA_DIR, filename)
    if not os.path.isfile(filepath):
        filepath = download_library(name)
    return filepath

def parse_gmt(filepath, namespace=None):
    d_gmt = {}
    with open(filepath, 'r') as f:
        for line in f:
            sl = line.strip().split('\t')
            term = sl[0]
            if namespace:
                term = '%s$%s' % (namespace, term)
            genes = [s for s in sl[2:] if s not in set(['', '---'])]
            d_gmt[term] = genes
    return d_gmt

def gmt2mat(d_gmt, sparse_output=False, min_frequency=0):
    '''Convert a dict to a bool matrix.'''
    enc = MultiLabelBinarizer(sparse_output=sparse_output)
    mat = enc.fit_transform(list(d_gmt.values()))
    genes = enc.classes_
    if min_frequency > 0:
        # filter out low freq genes
        mask = mat.sum(axis=0) > min_frequency
        mat = mat[:, mask]
        genes = genes[mask]
    
    return mat, genes

