'''
Helpers for interacting with Word2Vec
'''
import os
import re
import numpy as np
import pandas as pd

from gensim.test.utils import common_texts, get_tmpfile
from gensim.models import Word2Vec
import gensim.utils
import nltk.data
from nltk.tokenize import (RegexpTokenizer, WordPunctTokenizer, TreebankWordTokenizer)

# nltk.download('punkt')
sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

def document_preprocess(doc):
    '''Tokenize doc into sentences, 
    then tokenize each sentence into tokens by whitespaces.
    '''
    sentences = sent_detector.tokenize(doc['title_abstract'].strip())
    tokenizer = RegexpTokenizer(r"(?u)\b\w\w+\b")
    tokens = [tokenizer.tokenize(sent.lower()) for sent in sentences]
    return tokens


def load_serialno_gene_symbol_dict():
    '''Load serialno mappings used for tagger'''
    # This file is writen by `map_ensembl_ids.R`
    filepath = os.path.join(SCRIPT_DIR, '../data/tagger/parsed_dict/entities_human_genes_mapped_to_symbol-STRING.csv')
    entities_df = pd.read_csv(filepath)
    print(entities_df.shape)
    # Take the first symbol for serialno with multiple ensemble ids and multiple symbols   
    entities_df = entities_df.sort_values('ensemblid').groupby(['serialno']).first()
    print(entities_df.shape)
    d_serialno_gene = dict(zip(entities_df.index.astype(np.str), entities_df['hgnc_symbol']))
    return d_serialno_gene 


def parse_vocabs(vocabs, d_serialno_gene):
    '''Use Regex to find genes, then map them to gene symbols.
    @param:
        - vocabs: Word2Vec.wv.vocab
    '''
    gene_symbols = [None] * len(vocabs)
    token_counts = np.zeros(len(vocabs), dtype=np.int)
    i = 0
    for token, keyed_vec in vocabs.items():
        matched = re.fullmatch(r'^[0-9]{7,9}$', token)        
        token_counts[i] = keyed_vec.count
        if matched:
            serial_no = matched.string
            gene_symbol = d_serialno_gene.get(serial_no, serial_no)
            gene_symbols[i] = gene_symbol
        i += 1
        
    vocab_df = pd.DataFrame({
        'token': list(vocabs.keys()),
        'gene': gene_symbols,
        'count': token_counts
    })
    vocab_df['gene'].fillna(value=pd.np.nan, inplace=True)
    vocab_df['word'] = vocab_df['gene'].copy()
    vocab_df.loc[vocab_df['gene'].isnull(), 'word'] = vocab_df.loc[vocab_df['gene'].isnull(), 'token']    
    return vocab_df.reset_index()


def parse_model(model):
    '''Parse word2vec model to retrieve the w2v matrix 
    and the DataFrame of vocabs.
    '''
    vocabs = model.wv.vocab # a dict {token: gensim.models.keyedvectors.Vocab }
    wv_mat = np.asarray([model.wv.word_vec(token) for token in vocabs.keys()])
    d_serialno_gene = load_serialno_gene_symbol_dict()
    vocab_df = parse_vocabs(vocabs, d_serialno_gene)
    return wv_mat, vocab_df

def get_gene_vec_df(wv_mat, vocab_df):
    '''Organize wv_mat into a DataFrame of gene vectors.'''
    # Organize word2vec matrix for genes
    mask_gene = ~vocab_df.gene.isnull()
    vocab_gene_df = vocab_df.loc[mask_gene]
    genes = vocab_gene_df['gene'].values
    print(genes.shape)
    gene_vec_mat = wv_mat[mask_gene]
    print(gene_vec_mat.shape)
    gene_vec_df = pd.DataFrame(gene_vec_mat, index=genes)
    # Some symbols have multiple serial #
    # vocab_gene_df.gene.value_counts().head()
    # Take the first vector
    gene_vec_df = gene_vec_df.reset_index().groupby('index').first()
    print(gene_vec_df.shape)
    return gene_vec_df

