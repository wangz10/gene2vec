import numpy as np
import pandas as pd
from Bio import Entrez, Medline
Entrez.email = "zichen.wang@mssm.edu"

def retrieve_pubmed_abstracts(idlist):
    idlist = list(map(str, idlist))
    handle = Entrez.efetch(db="pubmed", id=idlist, 
                        rettype="medline",
                        retmode="text")
    records = Medline.parse(handle)    
    return list(records)


def replace_tagged_terms(records, tagger_results):
    '''Construct title + abstract document for each pmid, 
    then replace the matched terms with serialno
    '''
    # group tagging results by pmid
    grouped = tagger_results.groupby('pmid')
    pmids_with_entities = grouped.groups

    parsed_records = []
    # iterate over all pmids in records
    for record in records:
        if 'PMID' in record:
            pmid = int(record['PMID'])
            if pmid in pmids_with_entities:
                # convert to list of chars for replacement
                title_abstract = list(record['TI'] + ' ' + record['AB'])
                # replace named-entities with its serialno
                tagged_res_sub = grouped.get_group(pmid)
                for _, row in tagged_res_sub.iterrows():
                    start_idx = row['first_char']
                    end_idx = row['last_char']
                    title_abstract[start_idx] = str(row['serialno'])
                    title_abstract[start_idx+1: end_idx+1] = [''] * (end_idx - start_idx)
                # convert back to string
                title_abstract = ''.join(title_abstract)
                record['title_abstract'] = title_abstract
            record['PMID'] = int(record['PMID'])
            parsed_records.append(record)
    return parsed_records

