'''
Tensorflow implementation of unordered word2vec algorithm as a scikit-learn like model 
with fit, transform methods.

@author: Zichen Wang (wangzc921@gmail.com)

@references:

https://www.kaggle.com/c/word2vec-nlp-tutorial/details/
https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/word2vec/word2vec_basic.py
https://github.com/wangz10/UdacityDeepLearning/blob/master/5_word2vec.ipynb
'''

from __future__ import absolute_import
from __future__ import print_function

import collections
import math
import os
import random
import re
import json
from itertools import compress, permutations

import numpy as np
import tensorflow as tf
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.metrics.pairwise import pairwise_distances

# Set random seeds
SEED = 2018
random.seed(SEED)
np.random.seed(SEED)


#################### Util functions #################### 

# Global vars
gene_permutation = None
doc_index = 0

def compute_total_combinations(corpus):
    num = 0 
    for i in range(corpus.shape[0]):
        n_genes = corpus[i].nnz
        num += n_genes * (n_genes - 1)
    return num

def get_gene_permutations(corpus, i):
    _, genes = corpus[i].nonzero()
    return permutations(genes, 2)

def generate_batch_unorder(corpus, batch_size):
    '''
    Batch generator for Unordered 

    Parameters
    ----------
    corpus: scipy.sparse term-frequency matrix representation of a gene-set library with shape (n_terms, n_genes)
    batch_size: number of words in each mini-batch
    '''
    global gene_permutation
    global doc_index
    i = 0
    batch = np.ndarray(shape=(batch_size), dtype=np.int32)
    labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
    if gene_permutation is None:
        gene_permutation = get_gene_permutations(corpus, doc_index)

    while i < batch_size:
        try:
            gene_i, gene_j = next(gene_permutation)
        except StopIteration:
            if doc_index < corpus.shape[0] - 1:
                doc_index += 1
            else:
                doc_index = 0
            gene_permutation = get_gene_permutations(corpus, doc_index)
            gene_i, gene_j = next(gene_permutation)
        finally:
            batch[i] = gene_i
            labels[i, 0] = gene_j
            i += 1

    return batch, labels

class Word2VecUnorder(BaseEstimator, TransformerMixin):

    def __init__(self, batch_size=128, 
        # num_skips=2, skip_window=1, 
        embedding_size=128, 
        vocabulary_size=50000, 
        loss_type='sampled_softmax_loss', n_neg_samples=64,
        optimize='Adagrad', 
        learning_rate=1.0, 
        # n_steps=100001,
        valid_size=16, valid_window=100):
        # bind params to class
        self.batch_size = batch_size
        # self.num_skips = num_skips
        # self.skip_window = skip_window
        self.embedding_size = embedding_size
        self.vocabulary_size = vocabulary_size
        self.loss_type = loss_type
        self.n_neg_samples = n_neg_samples 
        self.optimize = optimize
        self.learning_rate = learning_rate
        # self.n_steps = n_steps
        # self.valid_size = valid_size
        # self.valid_window = valid_window
        # pick a list of words as validataion set
        # self._pick_valid_samples()
        # choose a batch_generator function for feed_dict
        self._choose_batch_generator()
        # init all variables in a tensorflow graph
        self._init_graph()

        # create a session
        self.sess = tf.Session(graph=self.graph)
        # self.sess = tf.InteractiveSession(graph=self.graph)


    def _choose_batch_generator(self):
        self.generate_batch = generate_batch_unorder

    def _init_graph(self):
        '''
        Init a tensorflow Graph containing:
        input data, variables, model, loss function, optimizer
        '''
        self.graph = tf.Graph()
        with self.graph.as_default():
            # Input data.
            self.train_dataset = tf.placeholder(tf.int32, shape=[self.batch_size])
            self.train_labels = tf.placeholder(tf.int32, shape=[self.batch_size, 1])
            # self.valid_dataset = tf.constant(self.valid_examples, dtype=tf.int32)

            # Variables.
            self.embeddings = tf.Variable(
                tf.random_uniform([self.vocabulary_size, self.embedding_size], -1.0, 1.0))

            self.weights = tf.Variable(
                tf.truncated_normal([self.vocabulary_size, self.embedding_size],
                    stddev=1.0 / math.sqrt(self.embedding_size)))

            self.biases = tf.Variable(tf.zeros([self.vocabulary_size]))

            # Model.
            # Look up embeddings for inputs.
            self.embed = tf.nn.embedding_lookup(self.embeddings, self.train_dataset)
            
            # Compute the loss, using a sample of the negative labels each time.
            if self.loss_type == 'sampled_softmax_loss':
                loss = tf.nn.sampled_softmax_loss(weights=self.weights, 
                    biases=self.biases, 
                    labels=self.train_labels,
                    inputs=self.embed,
                    num_sampled=self.n_neg_samples, 
                    num_classes=self.vocabulary_size)
            elif self.loss_type == 'nce_loss':
                loss= tf.nn.nce_loss(self.weights, self.biases, self.embed, 
                    self.train_labels, self.n_neg_samples, self.vocabulary_size)
            self.loss = tf.reduce_mean(loss)

            # Optimizer.
            if self.optimize == 'Adagrad':
                self.optimizer = tf.train.AdagradOptimizer(self.learning_rate).minimize(loss)
            elif self.optimize == 'SGD':
                self.optimizer = tf.train.GradientDescentOptimizer(self.learning_rate).minimize(loss)

            # Compute the similarity between minibatch examples and all embeddings.
            # We use the cosine distance:
            norm = tf.sqrt(tf.reduce_sum(tf.square(self.embeddings), 1, keep_dims=True))
            self.normalized_embeddings = self.embeddings / norm
            # self.valid_embeddings = tf.nn.embedding_lookup(
            # 	self.normalized_embeddings, self.valid_dataset)
            # self.similarity = tf.matmul(self.valid_embeddings, tf.transpose(self.normalized_embeddings))

            # init op 
            self.init_op = tf.initialize_all_variables()
            # create a saver 
            self.saver = tf.train.Saver()

    def fit(self, corpus, n_steps=100001, eval_every_n=5000):
        '''
        corpus: scipy.sparse term-frequency matrix representation of a gene-set library with shape (n_terms, n_genes).
        '''

        # with self.sess as session:
        session = self.sess

        session.run(self.init_op)
        # tf.initialize_all_variables().run()

        average_loss = 0
        print("Initialized")
        for step in range(n_steps):
            batch_data, batch_labels = self.generate_batch(corpus,
                self.batch_size)
            feed_dict = {self.train_dataset : batch_data, self.train_labels : batch_labels}
            op, l = session.run([self.optimizer, self.loss], feed_dict=feed_dict)
            average_loss += l
            if step % eval_every_n == 0:
                if step > 0:
                    average_loss = average_loss / eval_every_n
                # The average loss is an estimate of the loss over the last eval_every_n batches.
                print('Average loss at step %d: %f' % (step, average_loss))
                average_loss = 0

        # final_embeddings = self.normalized_embeddings.eval()
        final_embeddings = session.run(self.normalized_embeddings)
        self.final_embeddings = final_embeddings

        return self

    def save(self, path):
        '''
        To save trained model and its params.
        '''
        save_path = self.saver.save(self.sess, 
            os.path.join(path, 'model.ckpt'))
        # save parameters of the model
        params = self.get_params()
        json.dump(params, 
            open(os.path.join(path, 'model_params.json'), 'wb'))
        
        print("Model saved in file: %s" % save_path)
        return save_path

    def _restore(self, path):
        with self.graph.as_default():
            self.saver.restore(self.sess, path)

    @classmethod
    def restore(cls, path):
        '''
        To restore a saved model.
        '''
        # load params of the model
        path_dir = os.path.dirname(path)
        params = json.load(open(os.path.join(path_dir, 'model_params.json'), 'rb'))
        # init an instance of this class
        estimator = Word2VecUnorder(**params)
        estimator._restore(path)
        # evaluate the Variable normalized_embeddings and bind to final_embeddings
        estimator.final_embeddings = estimator.sess.run(estimator.normalized_embeddings)

        return estimator



