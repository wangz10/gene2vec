import os, sys
import numpy as np
import pandas as pd
import math
import geneshot
import tagger, pubmed

from pymongo import MongoClient
import pymongo

# client = MongoClient('mongodb://127.0.0.1:27017/')
client = MongoClient('mongodb://146.203.54.131:27017/')
db = client['topic-modeling']
coll = db['pubmed']
coll.create_index('PMID', unique=True)

# pmids_in_db = coll.distinct('PMID')
pmids_in_db = coll.aggregate([{'$group': {'_id': '$PMID'} }])
pmids_in_db = [doc['_id'] for doc in pmids_in_db]
print('PMIDs in DB: %d' % len(pmids_in_db))


autorif_df = pd.read_csv('../data/autorif.tsv', 
                         names=['gene', 'pmid', 'date'], 
                         sep='\t')
print(autorif_df.shape)
# autorif_df.head()
all_pubmed_ids = autorif_df['pmid'].unique()
print('PMIDs in AutoRIF: %d' % len(all_pubmed_ids))

all_pubmed_ids = list(set(all_pubmed_ids) - set(pmids_in_db))
print('PMIDs to retrieve DB: %d' % len(all_pubmed_ids))

n_pmids = len(all_pubmed_ids)

batch_size = 5000
n_batches = int(math.ceil(n_pmids / batch_size))

for i in range(n_batches):
    print('=== Batch %d ===' % i)
    start_idx = i * batch_size
    end_idx = (i+1) * batch_size

    pmids = all_pubmed_ids[start_idx: end_idx]

    print('Retrieving abstracts from PubMed...')
    # try:
    records = pubmed.retrieve_pubmed_abstracts(pmids)
    print(len(records))

    doc_stream = tagger.make_document(records)

    print('Doing tagging...')
    # parse with parsed dicts + stopwords
    output, err = tagger.call_tagger(doc_stream,
        tagger_dir='../data/tagger/parsed_dict',
        entities='entities_mammals.tsv',
        names='names_mammals.tsv',
        types='types_mammals.tsv',
        stopwords='stopwords.tsv',
        threads=8
        )

    output_df = tagger.parse_output(output, err)
    print('Finished tagging, NEs found: %d' % output_df.shape[0])
    
    records = pubmed.replace_tagged_terms(records, output_df)

    coll.insert_many(records)
    # except:
    #     pass
