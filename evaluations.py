'''
Utils for evaluating various types of gene2vec embeddings
'''
from itertools import combinations
import numpy as np
import pandas as pd
import scipy.sparse as sp
from sklearn.model_selection import cross_val_score
from sklearn import metrics
from sklearn.dummy import DummyClassifier
from sklearn.neighbors import KNeighborsClassifier

# Split data points into 80% training and 20% testing
def NN_generalization_accuracy(X, Y, dummy=False, scoring='accuracy'):
    '''Train 1-NN classifier on the training set of the low-dim data,
    and evaluate on the test set to get the accuracy.
    '''
    if not dummy:
        clf = KNeighborsClassifier(n_neighbors=1, metric='cosine', n_jobs=8)
    else:
        clf = DummyClassifier(strategy='stratified')
    # 5-fold cross-validation accuracy
    accuracy_scores = cross_val_score(clf, X, Y, cv=5, scoring=scoring)
    return accuracy_scores

def NN_generalization_accuracy_wrapper(X, libs_y, mat, libraries=None, dummy=False, model_name='', scoring='accuracy'):
    '''A wrapper for `NN_generalization_accuracy` when there are a list of libraries for eval.
    @params:
        - X: genes by topics matrix
        - libs_y: library names as target matrix for prediction
        - mat: the entire binary matrix (genes by terms)
        - libraries: array-like libraries corresponding to columns in mat
    '''
    knn_scores = []
    for lib in libs_y:
        mask_lib_y = np.in1d(libraries, [lib])
        Y = mat[:, mask_lib_y] # genes by functions binary matrix
        if sp.issparse(Y):
            Y = Y.toarray() 
        print(lib, Y.shape)
        
        scores = NN_generalization_accuracy(X, Y, dummy=dummy, scoring=scoring)
        knn_scores.append(scores)
    
    scores_df = pd.DataFrame({
        scoring: np.hstack(knn_scores), 
        'test_library': np.repeat(libs_y, 5)
    })
    scores_df['model'] = model_name
    return scores_df

def topic_coherence_pmi(topic, corpus_tf_mat, agg='mean'):
    '''Compute topic coherence by pointwise mutural information(PMI) for one topic.
    @params:
        - topic: a list of indices of tokens
        - corpus_tf_mat: a corpus represented by scipy.sparse matrix with shape: (n_documents, n_tokens)
    '''
    n_documents, n_tokens = corpus_tf_mat.shape
    token_count = corpus_tf_mat.sum()
    pmis = []

    # a vector of probabilities for each token
    token_probs = np.squeeze(np.asarray(corpus_tf_mat.sum(axis=0))) / token_count

    for i, j in combinations(topic, 2):
        p_i = token_probs[i]
        p_j = token_probs[j]
        if p_i * p_j == 0: # avoid zero division
            pmi = 0
        else:
            # find documents where both tokens exist
            ij_docs = (corpus_tf_mat[:, [i, j]] > 0).sum(axis=1)
            p_ij = ij_docs.sum() / n_documents
            if p_ij == 0: # avoid -inf
                pmi = 0
            else:
                pmi = np.log(p_ij / (p_i * p_j))

        pmis.append(pmi)
    
    if agg == 'mean':
        return np.mean(pmis)
    else:
        return np.median(pmis)

def topics_coherence_pmis(X, corpus_tf_mat, topn=10, agg='mean', dummy=False):
    '''Compute PMIs for all topics given genes by topics matrix.
    @params:
        - X: genes by topics matrix
    '''
    # make sure the number of genes/tokens are the same
    assert X.shape[0] == corpus_tf_mat.shape[1]
    n_genes = X.shape[0]
    n_topics = X.shape[1]
    pmis = np.zeros(n_topics)
    for j in range(n_topics):
        if dummy: # use random genes for this topic
            top_genes_idx = np.random.choice(n_genes, topn, replace=False)
        else:
            top_genes_idx = X[:, j].argsort()[::-1][:topn]
        pmis[j] = topic_coherence_pmi(top_genes_idx, corpus_tf_mat)
    return pmis


def topics_coherence_pmis_wrapper(X, libs_y, mat, libraries=None, dummy=False, model_name='', topn=10):
    all_pmis = []
    n_topics = X.shape[1]
    for lib in libs_y:
        mask_lib_y = np.in1d(libraries, [lib])
        Y = mat[:, mask_lib_y].T # functions by genes binary matrix
        print(lib, Y.shape)
        pmis = topics_coherence_pmis(X, Y, topn=topn, dummy=dummy)
        all_pmis.append(pmis)
    
    scores_df = pd.DataFrame({
        'PMI': np.hstack(all_pmis), 
        'test_library': np.repeat(libs_y, n_topics)
    })
    scores_df['model'] = model_name
    return scores_df

